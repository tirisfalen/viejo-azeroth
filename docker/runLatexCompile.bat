@echo off
cls

set TEX_FILE=main
set DOCKER_IMAGE=debian:latex
set HOST_FOLDER=%cd%
set DOCKER_FOLDER=/shared

:check_tex_file
    if not exist %TEX_FILE%.tex (
        echo El archivo %TEX_FILE%.tex no se encuentra en la carpeta actual.
        echo Por favor, comprueba que has ejecutado este script desde la carpeta base del proyecto o modifica la variable HOST_FOLDER por la ruta completa al archivo.
        pause
        exit /b
    )


:latex_compile
    if exist %TEX_FILE%.pdf (
        rm %TEX_FILE%.pdf
    )
    rem Por alguna razon el fichero resultante no compila la tabla del guerrero. Pendiente de arreglar
    docker run --rm -v %HOST_FOLDER%:%DOCKER_FOLDER% %DOCKER_IMAGE% lualatex %TEX_FILE%.tex
    cls
    echo "Compilado finalizado"

:clean_temp_files
    set /p COMPRESS_PROMPT="Desea borrar los archivos temporales de latex? (y/N): "
    if /i "%COMPRESS_PROMPT%"=="y" (
        cls
    	echo "Limpiando ficheros temporales..."
        rm *.aux
        rm *.log
        rm *.out
        rm *.toc
    )

:compress
    set /p COMPRESS_PROMPT="Desea comprimir el archivo compilado? (y/N): "
    if /i "%COMPRESS_PROMPT%"=="y" (
        cls
        set /p PDF_VERSION="Por favor, ingrese el nombre de la version (vX.X.X): "
        set OUTPUT_FILENAME=ViejoAzeroth_v%PDF_VERSION%.pdf
        docker run --rm -v %HOST_FOLDER%:%DOCKER_FOLDER% %DOCKER_IMAGE% gs -sDEVICE=pdfwrite -dNEWPDF=false -dCompatibilityLevel=1.5 -dPrinted=false -dPDFSETTINGS=/ebook -dNOPAUSE -dBATCH -sOutputFile=%OUTPUT_FILENAME% %TEX_FILE%.pdf
        cls
        echo %OUTPUT_FILENAME% terminado.
    )

pause